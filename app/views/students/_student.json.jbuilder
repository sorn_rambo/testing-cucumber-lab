json.extract! student, :id, :studentid, :name, :project_id, :created_at, :updated_at
json.url student_url(student, format: :json)
